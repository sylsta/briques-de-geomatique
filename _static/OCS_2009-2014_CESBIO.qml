<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" version="3.16.1-Hannover" styleCategories="AllStyleCategories" maxScale="0" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal fetchMode="0" mode="0" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <property value="false" key="WMSBackgroundLayer"/>
    <property value="false" key="WMSPublishDataSourceUrl"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property value="Value" key="identify/format"/>
  </customproperties>
  <pipe>
    <provider>
      <resampling zoomedInResamplingMethod="nearestNeighbour" enabled="false" zoomedOutResamplingMethod="nearestNeighbour" maxOversampling="2"/>
    </provider>
    <rasterrenderer type="paletted" alphaBand="-1" nodataColor="" opacity="1" band="1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
        <paletteEntry color="#ff5500" value="11" alpha="255" label="Cultures d'été"/>
        <paletteEntry color="#ffff7f" value="12" alpha="255" label="Cultures d'hiver"/>
        <paletteEntry color="#009c00" value="31" alpha="255" label="Forêt de feuillus"/>
        <paletteEntry color="#003200" value="32" alpha="255" label="Forêt de conifères"/>
        <paletteEntry color="#aaff00" value="34" alpha="255" label="Pelouses"/>
        <paletteEntry color="#55aa7f" value="36" alpha="255" label="Landes ligneuses"/>
        <paletteEntry color="#ff00ff" value="41" alpha="255" label="Urbain dense"/>
        <paletteEntry color="#ff55ff" value="42" alpha="255" label="Urbain diffus"/>
        <paletteEntry color="#ffaaff" value="43" alpha="255" label="Zones industrielles et commerciales"/>
        <paletteEntry color="#00ffff" value="44" alpha="255" label="Routes"/>
        <paletteEntry color="#ff0000" value="45" alpha="255" label="Surfaces minérales"/>
        <paletteEntry color="#ffb802" value="46" alpha="255" label="Plages et dunes"/>
        <paletteEntry color="#0000ff" value="51" alpha="255" label="Eau"/>
        <paletteEntry color="#bebebe" value="53" alpha="255" label="Glaciers ou neige"/>
        <paletteEntry color="#aaaa00" value="211" alpha="255" label="Prairies"/>
        <paletteEntry color="#aaaaff" value="221" alpha="255" label="Vergers"/>
        <paletteEntry color="#550000" value="222" alpha="255" label="Vignes"/>
      </colorPalette>
      <colorramp type="randomcolors" name="[source]"/>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0" gamma="1"/>
    <huesaturation grayscaleMode="0" colorizeStrength="100" saturation="0" colorizeGreen="128" colorizeOn="0" colorizeBlue="128" colorizeRed="255"/>
    <rasterresampler maxOversampling="2"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
