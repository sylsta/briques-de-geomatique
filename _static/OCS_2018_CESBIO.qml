<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" minScale="1e+8" version="3.4.4-Madeira" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property value="false" key="WMSBackgroundLayer"/>
    <property value="false" key="WMSPublishDataSourceUrl"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property value="Value" key="identify/format"/>
  </customproperties>
  <pipe>
    <rasterrenderer band="1" type="paletted" opacity="1" alphaBand="-1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
        <paletteEntry alpha="255" value="1" color="#ff00ff" label="Urbain dense"/>
        <paletteEntry alpha="255" value="2" color="#ff55ff" label="Urbain diffus"/>
        <paletteEntry alpha="255" value="3" color="#ffaaff" label="Zones indus. et comm."/>
        <paletteEntry alpha="255" value="4" color="#00ffff" label="Routes"/>
        <paletteEntry alpha="255" value="5" color="#ffff00" label="Colza"/>
        <paletteEntry alpha="255" value="6" color="#d0ff00" label="Ceréales à paille"/>
        <paletteEntry alpha="255" value="7" color="#a1d600" label="Protéagineux"/>
        <paletteEntry alpha="255" value="8" color="#ffaa44" label="Soja"/>
        <paletteEntry alpha="255" value="9" color="#d6d600" label="Tournesol"/>
        <paletteEntry alpha="255" value="10" color="#ff5500" label="Maïs"/>
        <paletteEntry alpha="255" value="11" color="#c5ffff" label="Riz"/>
        <paletteEntry alpha="255" value="12" color="#aaaa61" label="Tubercules/Racines"/>
        <paletteEntry alpha="255" value="13" color="#aaaa00" label="Prairies"/>
        <paletteEntry alpha="255" value="14" color="#aaaaff" label="Vergers"/>
        <paletteEntry alpha="255" value="15" color="#550000" label="Vignes"/>
        <paletteEntry alpha="255" value="16" color="#009c00" label="Forêts de feuillus"/>
        <paletteEntry alpha="255" value="17" color="#003200" label="Forêts de conifères"/>
        <paletteEntry alpha="255" value="18" color="#aaff00" label="Pelouses"/>
        <paletteEntry alpha="255" value="19" color="#55aa7f" label="Landes ligneuses"/>
        <paletteEntry alpha="255" value="20" color="#ff0000" label="Surfaces minérales"/>
        <paletteEntry alpha="255" value="21" color="#ffb802" label="Plages et dunes"/>
        <paletteEntry alpha="255" value="22" color="#bebebe" label="Glaciers et neiges éternelles"/>
        <paletteEntry alpha="255" value="23" color="#0000ff" label="Eau"/>
        <paletteEntry alpha="0" value="0" color="#0000ff"/>
      </colorPalette>
      <colorramp type="randomcolors" name="[source]"/>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeGreen="128" colorizeBlue="128" saturation="0" colorizeStrength="100" colorizeOn="0" grayscaleMode="0" colorizeRed="255"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
