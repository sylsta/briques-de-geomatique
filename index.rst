.. Briques de Géomatique documentation master file, created by
   sphinx-quickstart on Wed Jan  6 17:16:10 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

|logo-up|				|logo-prodig|				|logo-geoteca|

.. |logo-up| image:: figures/logo_universite_de_paris.png
              :width: 10 em
              :target: https://u-paris.fr/

.. |logo-prodig| image:: figures/logo_PRODIG.png
              :width: 5 em
              :target: https://www.prodig.cnrs.fr/

.. |logo-geoteca| image:: figures/logo_GEOTECA.png
              :width: 15 em
              :target: http://geoteca.u-paris.fr/

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Bienvenue sur les Briques de Géomatique !
==========================================

Cette documentation a pour but de présenter quelques manipulations courantes (et moins courantes) de géomatique avec différents outils. L'idée est de présenter des *briques élémentaires* avec une entrée mêlant étroitement théorie et pratique. Chaque utilisateur est invité à appliquer et à assembler ces briques à son propre projet géomatique.

L'approche se veut par besoin clef : *reprojeter un raster / vecteur*, *construire une composition colorée*, *faire une jointure attributaire*... et sans à priori logicielle (même si dans un premier temps une large part se fera avec QGIS). L'idée est d'expliquer brièvement en théorie chaque brique puis d'explorer leurs applications avec différents logiciels et langages.

Cette documentation est distribuée en licence CC BY-NC-SA 3.0 FR. Vous pouvez donc librement l'utiliser, la partager et la modifier.

Pour une lecture hors ligne, il est possible de télécharger la version PDF ou EPUB de cette documentation :

    * `PDF`_
    * `EPUB`_
    
.. _PDF: https://briques-de-geomatique.readthedocs.io/_/downloads/fr/latest/pdf/
.. _EPUB: https://briques-de-geomatique.readthedocs.io/_/downloads/fr/latest/epub/

**Introduction à la télédétection**

Ce chapitre d'*Introduction* est consacrée à la présentation théorique de la télédétection, des grandes missions d'observation de la Terre et des sources de téléchargement des images satellites associées.

.. toctree::
   :maxdepth: 3

   telede-theorie-missions-donnees


**Logiciels et outils**

Ce chapitre sur les *Logiciels et outils* présente quelques logiciels et outils utiles à la géomatique et détaille leur procédure d’installation.

.. toctree::
   :maxdepth: 3
   
   logiciels-outils


**Format de données et import**

Ce chapitre consacré au *Format de données* présente les principaux formats de données utilisés en géomatique et la manière de procéder pour les importer dans les différents logiciels.

.. toctree::
   :maxdepth: 3
   
   format-donnees-import


**Systèmes de Coordonnées de Référence**

Ce chapitre présente quelques généralités théoriques sur les *Systèmes de Coordonnées de Référence* ainsi que quelques manipulations pratiques pour les manipuler.

.. toctree::
   :maxdepth: 3
   
   SCR-theorie-pratique


**Données existantes**

Ce chapitre sur les *Données existantes* regroupe les fiches présentant les différentes sources de données *prêtes à l'emploi* déjà existantes.

.. toctree::
   :maxdepth: 3
   
   donnees-pretes


**Création de données**

Ce chapitre sur la *Création de données* présente des outils pour créer ses propres données géospatiales.

.. toctree::
   :maxdepth: 3
   
   creation-de-donnees


**Tables attributaires**

Ce chapitre sur les *Tables attributaires* est consacrée à la présentation des outils portant sur l'édition des tables attributaires associées aux couches vecteurs.

.. toctree::
   :maxdepth: 3
   
   table-attributaire


**Géométries vectorielles**

Ce chapitre sur les *Géométries vectorielles* est consacrée à la présentation de quelques outils permettant de travailler sur les géométries des couches vecteurs.

.. toctree::
   :maxdepth: 3
   
   geometries-vecteur


**Sélections**

Ce chapitre consacré aux *Sélections* présente les techniques de sélections attributaires et spatiales.

.. toctree::
   :maxdepth: 3

   selections


**Outils rasters**
   
Ce chapitre sur les *Outils rasters* présente quelques outils utiles au traitement et à l'analyse de données rasters.

.. toctree::
   :maxdepth: 3
   
   outils-raster


**Combinaison raster et vecteur**

Ce chapitre sur la *Combinaison raster et vecteur* présente des outils permettant de faire dialoguer des couches rasters et vecteurs. 

.. toctree::
   :maxdepth: 3
   
   raster-vecteur


**Outils de télédétection**

Ce chapitre d'*Outils de télédétection* présente quelques manipulations de *base* à éventuellement effectuer avant des analyses de télédétection plus poussées.

.. toctree::
   :maxdepth: 3
   
   outils-teledetection


**Classification non supervisée**

Ce chapitre sur la *Classification non supervisée* présente la façon de procéder pour réaliser une classification non supervisée d'images satellites selon différents outils.

.. toctree::
   :maxdepth: 3
   
   classification-non-supervisee


**Classification supervisée**

Ce chapitre sur la *Classification supervisée* présente la façon de procéder pour réaliser une classification supervisée d'images satellites selon différents outils.

.. toctree::
   :maxdepth: 3
   
   classification-supervisee


**Post-traitement de classification**

Ce chapitre sur le *Post-traitement de Classification* présente quelques traitements utiles à appliquer à la suite d'une classification d'images satellites pour en améliorer l'interprétation.

.. toctree::
   :maxdepth: 3
   
   classification-post-traitement


**Segmenation d'images**

Ce chapitre sur la *Segmentation d'images* présente les principes et les techniques de segmentations d'images à utiliser pour extraire des informations à partir notamment d'images à très haute résolution spatiale.

.. toctree::
   :maxdepth: 3
   
   segmentation-images

**Géomorphologie**

Ce chapitre consacré à la *Géomorphologie* présente quelques outils à appliquer à des modèles numériques de terrain pour en tirer des informations éclairant sur la géomorphologie d'une zone d'étude. 

.. toctree::
   :maxdepth: 3
   
   geomorpho


**Terrain**

Ce chapitre *Terrain* présente des outils permettant de capturer des données géospatiales directement depuis son terrain d'étude.

.. toctree::
   :maxdepth: 3
   
   terrain


**Cartographie et visualisation**

Ce chapitre *Cartographie et visualisation* présente comment produire des cartes et des visualisations simples à partir de ses données.

.. toctree::
   :maxdepth: 3
   
   rendu-carto


**Divers**

Ce chapitre *Divers* présente des manipulations qui ne sont pas propres à la géomatique mais qui peuvent s'avérer utile dans un contexte d'analyse de données spatialisées.

.. toctree::
   :maxdepth: 3
   
   divers
   citation
   auteurs

..
	Index and tables
	==================
..
	* :ref:`genindex`
	* :ref:`modindex`
	* :ref:`search`

Une suggestion ? Un commentaire ? `Contactez moi`_.

.. _Contactez moi: paul.passy@u-paris.fr

.. raw:: html

    <embed>
        <center><a href="https://livetrafficfeed.com/website-counter" data-time="Europe%2FParis" data-root="0" id="LTF_counter_href">Free Start Counter</a><script type="text/javascript" src="//cdn.livetrafficfeed.com/static/static-counter/live.v2.js"></script><noscript><a href="https://livetrafficfeed.com/website-counter">Free Start Counter</a></noscript></center>
    </embed>
