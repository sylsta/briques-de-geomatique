..  _table-attributaire:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Travailler sur les tables attributaires
========================================

Comme vu dans la partie de présentation des différents formats de données, les données vecteurs sont caractérisées par la présence d'une table attributaire. Ces tables attributaires sont ds éléments primordiaux des SIG. Cette partie présente quelques manipulations à savoir faire sur ces tables. Ces manipulations vont du calcul de champ à la mise à jour d'un champ en passant par la suppression ou le formatage d'un champ (et autres).

.. contents:: Table des matières
    :local:

..  _calcul-champ:

Calcul de champs
----------------

Les tables attributaires associées aux données vecteurs sont des objets fondamentaux en géomatique. Ces objets sont sans cesse modifier au cours du travail. Dans cette section nous allons voir comment mettre à jour une table attributaire en ajoutant un nouveau champ et en mettant à jour ce champ selon un calcul défini par l'utilisateur.

..  _calcul-champ-qgis:

Calcul de champs dans QGIS
****************************
Version de QGIS : 3.16.1

Dans QGIS, la mise à jour d'un champ est assez simple et passe par la *Calculatrice de champ*. Un champ peut être mis à jour avec une valeur unique, en fonction de valeurs contenues dans d'autres champs (comme un calcul de densité de population à partir de champs "population" et "superficie" préexistants) ou en fonction de la géométrie de chaque entité (comme un calcul de superficies, périmètres ...)

Dans cet exemple, nous allons simplement calculer la superficie en kilomètres carrés de chaque bassin-versant unitaire du bassin de la Seine. Nous travaillerons sur la couche nommée *hydro_bv_seine*.

Une fois la couche chargée dans QGIS, nous sélectionnons notre couche dans le panneau des couches et nous ouvrons la *Calculatrice de champ* en cliquant sur l'icône |icone-calcul-champ|. Le menu correspondant apparaît (:numref:`calculatrice-champ`).

.. |icone-calcul-champ| image:: figures/icone_calculatrice_champ.png
              :width: 25 px

.. figure:: figures/fen_calculatrice_champ.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: calculatrice-champ
    
    Calcul d'un nouveau champ avec QGIS.

Nous spécifions tout d'abord que nous créons un nouveau champ en sélectionnant ``Créer un nouveau champ``. Notons qu'il serait également possible de mettre à jour un champ existant en sélectionnant ``Mise à jour d'un champ existant``. Dans le champ ``Nom``, nous baptisons notre champ ``superficie_km2``.

.. note::
  Il est bien d'inclure l'unité dans le nom des champs.

Dans le champ ``Type``, nous spécifions le type du champ que nous allons créer. Ici, il s'agît d'une superficie, qui sera un nombre décimal. Nous sélectionnons donc ``Nombre décimal (réel)``.

.. note::
  Les différents types possibles dépendent du format du fichier que nous mettons à jour. Un GeoPackage accepte plus de formats différents qu'un shapefile par exemple.

Dans le panneau ``Expression``, nous allons entrer l'expression à utiliser pour le calcul du champ. Cette expression peut être écrite avec l'aide des fonctions données dans le panneau central. Pour la superficie, dans ce panneau central, nous déroulons le menu ``Géométrie`` et nous doubles cliquons sur ``$area``. Cette expression apparaît dans le panneau ``Expression``. Comme notre couche est en Lambert 93 dont l'unité est le *mètre*, il nous faut diviser la superficie par 1000000 pour obtenir une superficie en kilomètres carrés. Nous obtenons ainsi l'expression ``$area / 1000000``.

.. tip::
  Dans la majorité des cas, nous utilisons la fontion ``$area``, mais il existe aussi une fonction ``area``. Quelle est la différence entre les deux ? Dans le premier cas, la surface est calculée en prenant en compte l'ellipsoïde du SCR. Autrement dit, il s'agît d'une surface qui prend en compte la courbure de la Terre. Dans le second cas, la surface est calculée de façon planimétrique, comme si la Terre était plate. C'est pourquoi, nous utilisons le plus souvent la fonction ``$area`` qui donne des résultats plus proches de la réalité.

Une fois l'expression entrée, nous cliquons sur :guilabel:`OK`. Nous avons bien maintenant un champ de superficie associée à la table attributaire (:numref:`champ-sup`).

.. figure:: figures/fen_table_avec_superficie.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: champ-sup
    
    Le champ de superficie en kilomètres carrés.

Notons qu'au moment du calcul du champ, la couche passe automatiquement en *mode édition*. Il est ensuite nécessaire d'enregistrer la modification en cliquant sur l'icône *Enregistrer les modifications de la couche* |icone-enregistrer| puis de quitter le mode édition en cliquant sur l'icône *Basculer en mode édition* |icone-edition|.

.. tip::
  Dans la *Calculatrice de champ*, dans le panneau central nous trouvons le menu déroulant ``Récent (fieldcalc)`` qui contient l'historique des dernières expressions calculées. Ce menu s'avère très pratique à l'usage.

  
..  _calcul-champ-R:

Calcul de champs dans R
****************************
Version de R : 4.8.1

Après l'import d'un fichier vecteur dans R, il est possible de manipuler ses attributs et notamment d'en calculer de nouveaux. Ici, nous travaillerons sur un vecteur des départements de France métropolitaine de type polygone, stocké dans une variable nommé *dep* sous forme d'un objet *sf*. Cette couche est projetée en Lambert 93 (:ref:`SCR-nationaux`).

Ajout d'un champ texte
++++++++++++++++++++++++

Nous pouvons ajouter à chaque entité un attribut de type *texte* nommé *type_admin* qui contiendra simplement le terme *departement*. Cette manipulation se fait suivant la syntaxe classique de R.

.. code-block:: R

    dep$type_admin <- 'departement'

Par cette simple commande, un nouveau champ nommé *type_admin* sera ajouté et rempli selon la valeur désirée.

Ajout d'un champ lié à la géométrie
++++++++++++++++++++++++++++++++++++

Nous pouvons ajouter un champ lié à la géométrie de la couche, comme une superficie, une longueur, un couple (X, Y), ... Ces valeurs seront exprimées dans l'unité du système de coordonnées de référence de la couche. Il s'agît dans la plupart des cas de mètres, mais attention il peut également s'agir de degrés dans le cas des SCR globaux (:ref:`SCR-globaux`). Ici, nous allons ajouter un attribut, nommé *superficie*, qui stockera la superficie de chaque département exprimée en kilomètres carrés. Comme cette couche est en Lambert 93, son unité de base est le mètre. Il faudra bien penser à faire la conversion des m\ :sup:`2` vers les km\ :sup:`2`.

.. code-block:: R

    dep$superficie <- as.numeric(st_area(dep) / 1000000)

La méthode *sf* utilisée est ``st_area()``. Cette méthode renvoie la superficie de chacune des entités de la couche dans l'unité de référence de la couche. Nous divisons cette valeur par 1000000 afin de passer des superficies en m\ :sup:`2` vers des superficies en km\ :sup:`2`.

.. tip::
  Nous convertissons ce nouvel attribut en *numérique* via la méthode ``as.numeric()``. Si nous le le faisons pas, le champ ne sera pas de type numérique mais dans un type nommé *units* qui sera moins facile à manipuler. 

..  _renommer-champ:

Renommer un champ
---------------------

Les tables attributaires associées aux données vecteurs ne sont pas figées. L'utilisateur peut tout à fait renommer un champ pour rendre son intitulé plus explicite ou corriger une faute de frappe.

..  _renommer-champ-qgis:

Renommer un champ dans QGIS
*****************************
Version de QGIS : 3.16.1

La manipulation est simple, mais le chemin pour le faire ne se devine pas. Une fois la couche à modifier chargée dans QGIS, nous allons dans ses *Propriétés* en cliquant droit sur la couche en question dans le panneau des couches et en sélectionnant le menu ``Propriétés``. Une fois les propriétés ouvertes nous allons dans l'onglet ``Champs`` (:numref:`prop_champ`).

.. figure:: figures/fen_prop_champs_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: prop_champ
    
    Les propriétés des champs d'une couche vecteur.

Nous y trouvons la liste des champs de la couche vecteur. Nous trouvons notamment le ``Nom`` des champs qui est l'intitulé des champs. Il y a également le ``Type`` des champs. Ici, nous avons des champs de type *QString*, *Int* et même *QDateTime*. Dans la colonne d'à côté, nous avons le nom générique de ces types : *String* (texte), *Integer* (entier) et *DateTime* (date avec l'heure). Nous avons également la *Longueur* et la *Précision* des champs. Par exemple, le champ *CdOH* contient du texte qui ne peut pas contenir plus de 19 caractères.

Le nombre de types de champs possibles varient selon le format du fichier. Ici, il s'agît d'un GeoPackage, format qui propose une certaine variété de types possibles. Par contre, un shapefile sera plus limité. Le type *DateTime* ne sera, par exemple, pas possible dans ce format.

Via cet interface nous allons pouvoir renommer les champs que nous souhaitons. Pour cela, nous basculons en *mode édition* en cliquant sur l'icône correspondante |icone-edition|. Deux nouveaux menus deviennent accessibles, mais ils ne vont pas nous intéresser pour le renommage de champ. Nous allons simplement double cliquer sur le *Nom* du champ à modifier. Par exemple, nous allons renommer le champ *TopoOH* en *TopoStation*. Nous double cliquons sur le *Nom* de ce champ et nous avons accès à son édition (:numref:`renommer_champ`).

.. figure:: figures/fen_renommer_champ1_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: renommer_champ
    
    Renommer un champ.

Il suffit alors de taper le nom désiré à la place de l'ancien.

.. note::
	Notons bien que seul le nom du champ peut être changé. Son type n'est pas modifiable.

Une fois la modification effectuée, nous cliquons sur :guilabel:`Appliquer` puis :guilabel:`OK`. En ouvrant la table d'attributs de la couche, nous constatons bien que le nom a été modifié.

.. warning::
	Si nous travaillons sur un shapefile, n'oublions pas que le nom des champs est limité à 8 caractères seulement. 


..  _changer-format-champ:

Changer le format d'un champ
---------------------------------

Il arrive qu'il soit nécessaire de changer le format d'un champ, de le convertir dans un autre type. Par exemple, un champ que nous voudrions avoir au format *Entier* est en fait en format *Texte* et nous ne pouvons donc pas le manipuler comme nous le souhaiterions. Dans ce cas, il est possible de transformer ce champ de type *Texte* en type *Entier* (ou *Réel*).

Dans la plupart des cas il n'est pas possible de changer le type d'un champ. La stratégie consiste donc à créer un nouveau champ dans le type désiré puis de le mettre à jour en copiant les valeurs de l'ancien champ en les convertissant.

.. warning::
	Il n'est bien sûr pas possible de tout convertir en n'importe quoi. Le texte *95* pourra être converti en entier *95* et même en réel *95.0* mais le texte *Val-d'Oise* ne pourra évidemment pas être converti en format numérique. 

Dans les exemples suivants nous allons convertir le champ des codes INSEE des communes des Hauts-de-Seine (92) qui sont en *Texte* en type *Entier*. Cette transformation nous permettrait par exemple de pouvoir facilement sélectionner tous les départements dont leur numéro est supérieur à 90 (si nous disposions des départements pour toute la France).

.. contents:: Table des matières
    :local:

Convertir un champ dans QGIS
*********************************
Version de QGIS : 3.18.3

Dans QGIS la procédure à suivre est simple. Après avoir chargé la couche à modifier nous ouvrons sa table attributaire (:numref:`qgis-table`).

.. figure:: figures/fen_qgis_table_attributaire.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-table
    
    Ouverture de la table attributaire à modifier.

Ici nous nous intéressons au champ *INSEE_COMM*. À première vue il contient des nombres mais on s'aperçoit que c'est en fait un champ textuel car ces "nombres" sont alignés sur la gauche des cellules. Les "vrais" nombres sont alignés sur la droite de la cellule comme nous pouvons le voir dans les champs de coordonnées des chefs lieux X et Y.

Si le doute persiste nous pouvons faire un clic droit sur la couches dans le panneau des couches puis :menuselection:`Propriétés... --> Champs`. Le format des champs de la table attributaire s'affiche alors (:numref:`format-champ`).

.. figure:: figures/fen_qgis_format_champ.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: format-champ
    
    Format des champs de la table attributaire.

Nous constatons bien que le champ *INSEE_COM* est bien de type *String* (*Texte*, pas l'accessoire de mode...). Pour créer un nouveau champ qui contiendra ce code INSEE mais au format numérique *Entier* (*Integer*) nous sélectionnons la couche à modifier dans le panneau des couches et nous ouvrons la calculatrice de champ en cliquant sur l'icône ``Ouvrir la calculatrice de champ`` |icone_calc_champ|. La calculatrice de champ s'ouvre alors (:numref:`convert-champ`).

.. |icone_calc_champ| image:: figures/icone_calculatrice_champ.png
              :width: 20 px

.. figure:: figures/fen_qgis_convert_champ.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: convert-champ
    
    Création d'un nouveau champ avec un nouveau format.

Nous cochons la case ``Créer un nouveau champ``, dans le champ ``Nom``, nous spécifions un nom pour le champ qui sera créé, par exemple *INSEE_int* et dans le champ ``Type`` nous spécifions le type du champ qui sera créé, à savoir ``Nombre entier (entier)``. Dans le bloc ``Expression``, nous entrons l'expression qui permet la conversion

.. code-block:: sql

   to_int("INSEE_COM")

Nous pouvons nous aider de l'explorateur de fonctions du panneau central : ``Conversions`` et ``Champs et valeurs`` pour construire cette expression. Puis nous cliquons sur :guilabel:`OK`.

.. tip::
	Il est tout à fait possible de convertir ce champ *Texte* vers un nombre réel plutôt que vers un entier. Dans ce cas, nous mettons le type de champ à ``Nombre décimal (réel)`` et nous utilisons la fonction *to_real( )* plutôt que *to_int( )*.

En ouvrant la table attributaire nous constatons bien que le nouveau champ a été créé dans le bon format. Il ne reste plus qu'à quitter le mode édition de la couche et à la sauver.


..  _supprimer-champ:

Supprimer un champ
---------------------

Les tables attributaires associées aux données vecteurs ne sont pas figées. L'utilisateur peut tout à fait supprimer un champ qui s’avérerait inutile ou faux.

Supprimer un champ dans QGIS
*********************************
Version de QGIS : 3.16.1

La manipulation est très simple. Une fois notre couche vectorielle dont nous souhaitons supprimer un champ est chargée dans QGIS, nous commençons par ouvrir sa table attributaire. Une fois la table ouverte, nous passons en *mode édition* en cliquant sur l'icône |icone-edition| (:numref:`suppr-champ1`).

.. |icone-edition| image:: figures/icone_edition.png
              :width: 25 px

.. figure:: figures/fen_supprimer_champ1_qgis.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: suppr-champ1
    
    Ouverture de la table en mode édition.

Une fois le mode édition activé, de nouveaux menus deviennent accessibles dans la barre d'outils. Nous y retrouvons notamment le menu :guilabel:`Supprimer le champ` |icone-suppr-champ|. Nous cliquons dessus et le menu suivant s'ouvre (:numref:`suppr-champ2`).

.. |icone-suppr-champ| image:: figures/icone_supprimer_champ.png
              :width: 20 px

.. figure:: figures/fen_supprimer_champ2_qgis.png
    :width: 12em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: suppr-champ2
    
    Liste des champs à supprimer.

Ce menu liste tous les champs de la couche et invite l'utilisateur à sélectionner le ou les champs à supprimer. Par exemple, si nous souhaitons supprimer les champs *CdOH*, *TopoOH* et *CommentaireOH*, nous les sélectionnons en maintenant la touche ``Ctrl`` enfoncée (:numref:`suppr-champ3`).

.. figure:: figures/fen_supprimer_champ3_qgis.png
    :width: 12em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: suppr-champ3
    
    Sélection des champs à supprimer.

Une fois cette sélection effectuée, il ne reste plus qu'à cliquer sur :guilabel:`OK`. Nous revenons à la table attributaire et nous constatons que les champs en question ont bien été supprimés. Il ne reste plus qu'à cliquer sur :guilabel:`Enregistrer les modifications` |icone-enregistrer| et à sortir du mode édition en recliquant sur l'icône |icone-edition|.

.. |icone-enregistrer| image:: figures/icone_enregistrer.png
              :width: 20 px
